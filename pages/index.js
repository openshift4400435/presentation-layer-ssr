// pages/index.js
import { useState, useEffect } from 'react';

export default function Home() {
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [education, setEducation] = useState('');
  const [position, setPosition] = useState('');

  const handleAddEmployee = async () => {
    const response = await fetch('/api/add', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ name, phone, email, education, position }),
    });

    const data = await response.json();
    console.log(data); // You can handle the response accordingly
    fetchEmployees();
  };

  const [employees, setEmployees] = useState([]);

  const fetchEmployees = async () => {
    const response = await fetch('/api/all');
    const data = await response.json();
    setEmployees(data);
  };

  useEffect(() => {
    fetchEmployees();
  }, []);

  return (
    <div className="App">
      <h1>Employee Management System v3</h1>
      <div className="form">
        <input
          type="text"
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <input
          type="text"
          placeholder="Phone"
          value={phone}
          onChange={(e) => setPhone(e.target.value)}
        />
        <input
          type="text"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <input
          type="text"
          placeholder="Education"
          value={education}
          onChange={(e) => setEducation(e.target.value)}
        />
        <input
          type="text"
          placeholder="Position"
          value={position}
          onChange={(e) => setPosition(e.target.value)}
        />
        <button onClick={handleAddEmployee}>Add Employee</button>
      </div>
      <div className="employee-list">
        <h2>Employee List</h2>
        <ul>
          {employees.map((employee) => (
            <li key={employee.id}>
              Name: {employee.name}, Phone: {employee.phone}, Email: {employee.email}, Education: {employee.education}, Position: {employee.position}
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
