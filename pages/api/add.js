import axios from 'axios';

export default async function handler(req, res) {
    if (req.method === 'POST') {
        const { name, phone, email, education, position } = req.body;
        try {
            const response = await axios.post('http://application-layer.three-tier-application.svc.cluster.local:8080/employee/add', {
            name, phone, email, education, position
            });
            console.log(response.data);
        } catch (error) {
            res.status(500).json({ success: false, message: 'Error adding employee!' });
            console.error('Error adding employee:', error);
        }
        res.status(200).json({ success: true, message: 'Employee added successfully!' });
    } else {
      res.status(405).json({ success: false, message: 'Method not allowed.' });
    }
  }
  