// pages/api/employees.js
import axios from 'axios';

export default async function handler(req, res) {
  if (req.method === 'GET') {
    try {
      const response = await axios.get('http://application-layer.three-tier-application.svc.cluster.local:8080/employee/all');
      const employees = response.data;
      res.status(200).json(employees);
    } catch (error) {
      res.status(500).json({ error: 'Error fetching employees' });
    }
  } else {
    res.status(405).json({ error: 'Method not allowed' });
  }
}
